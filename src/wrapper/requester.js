import axios from "axios"

const DEBUG_REQUEST = process.env.DEBUG_REQUEST || true

/**
 * @param {String} method
 * @param {String} url
 * @param {Object} options : { data:{...}, headers:{...} }
 * @param {Function} callback : data => { ...sendData }
 */
export const Call = async (method, url, options, callback = false) =>
  axios({ method, url, options })
    .then(result => {
      //const { data, status, statusText, headers, config, request } = result
      Debug(result.data)

      callback ? callback(result.data) : false

      return Promise.resolve({ data: result.data, _hasError: false })
    })
    .catch(error => {
      const { request, response, config, ...result } = error

      Debug(result)

      callback ? callback(result) : false

      return Promise.resolve({ data: result, _hasError: true })
    })

export const Debug = data => {
  // check if debug enabled
  if (!DEBUG_REQUEST) {
    return false
  }

  return new Promise((resolve, reject) => {
    try {
      resolve()
      console.log({ requestDebug: true, data })
    } catch (error) {}
  })
}

export default Call
